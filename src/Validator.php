<?php

namespace Pumu\UuidApi;

interface Validator
{
    /**
     * @param string $uuid
     * @return bool
     */
    public function isValid(string $uuid): bool;
}
