<?php
namespace Pumu\UuidApi;

interface Uuid
{
    public function toString(): string;
}
