<?php

namespace Pumu\UuidApi;

interface Generator
{
    public function random(): Uuid;

    public function fromString(string $uuid): Uuid;
}
