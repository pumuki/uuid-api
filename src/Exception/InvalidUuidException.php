<?php
namespace Pumu\UuidApi\Exception;

use Throwable;

interface InvalidUuidException extends Throwable
{
}
